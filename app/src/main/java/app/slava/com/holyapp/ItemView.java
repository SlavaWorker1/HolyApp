package app.slava.com.holyapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemView extends FrameLayout {

    int parentWidth;
    int parentHeight;

    private Context cx;
    private ImageView imageView;

    public ItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ItemView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        this.cx = context;

        parentWidth = UtilsApp.parentWidth(context);
        parentHeight = UtilsApp.parentHeight(context);
    }


    public int getParentWidth() {
        return parentWidth;
    }

    public int getParentHeight() {
        return parentHeight;
    }

    public void addText(String textParam) {

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                parentWidth/3-2,
                LayoutParams.WRAP_CONTENT);
        TextView tv = new TextView(cx);
        if (parentWidth<500) tv.setTextSize(8);
        if (parentWidth>1000) tv.setTextSize(12);
        else tv.setTextSize(10);
        tv.setText(textParam);
        layoutParams.gravity = Gravity.BOTTOM;
            tv.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL);
        tv.setTextColor(Color.argb(255, 0, 0, 0));

        int paddings = 20;
        tv.setLayoutParams(layoutParams);

        tv.setPadding(paddings, paddings/2, paddings, paddings/2);

        tv.setBackgroundColor(Color.argb(180, 255, 255, 255));

        this.addView(tv);
    }

    public void addImage(Bitmap bitmap){

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                parentWidth/3-2,
                parentHeight/4);

        imageView = new ImageView(cx);
        imageView.setImageBitmap(bitmap);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
        imageView.setLayoutParams(layoutParams);

        this.addView(imageView);
    }

}