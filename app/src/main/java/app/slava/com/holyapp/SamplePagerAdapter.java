package app.slava.com.holyapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import app.slava.com.holyapp.orm.Theme;
import app.slava.com.holyapp.orm.Versus;

public class SamplePagerAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener {

    private List<Long> themesId = null;
    private Context context;
    private LayoutInflater inflater;
    private ImageView imgBackdrop;
    private ArrayList<Integer> temp;
    private Theme theme;
    int currentPosition;

    public SamplePagerAdapter(List<Long> themesId, Context context, Long currentPosition) {
        this.themesId = themesId;
        this.context = context;
        this.currentPosition = Integer.valueOf(String.valueOf(currentPosition));
        this.temp = new ArrayList<>();
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        super.finishUpdate(container);
    }



    @Override
    public int getItemPosition(Object object) {
        return POSITION_UNCHANGED;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        temp.add(position);

        Log.d("Position", String.valueOf(position));

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.frament_scrolling, null);

        imgBackdrop = (ImageView)itemView.findViewById(R.id.backdrop);

        theme = Theme.load(Theme.class, themesId.get(position));
        Bitmap img = UtilsApp.decodeSampledBitmapFromDrawable(
                context,
                UtilsApp.arrImages[(Integer.valueOf(String.valueOf(theme.getId())))],
                imgBackdrop.getMaxWidth(),
                imgBackdrop.getMaxHeight()
        );

        imgBackdrop.setImageBitmap(img);

        List<Versus> versusList = theme.versuses();
        LinearLayout linearLayout = (LinearLayout) itemView.findViewById(R.id.cardLayout);

        for (int i = 0; i < versusList.size(); i++) {
            Card card = new Card(context, versusList.get(i), linearLayout);
            LinearLayout.LayoutParams cardsParams = new AppBarLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );
            int margin = UtilsApp.convertDpToPixel(8, context);
            cardsParams.setMargins(margin, margin, margin, margin);
            card.getTvText().setText(versusList.get(i).text);
            card.setLayoutParams(cardsParams);

            linearLayout.addView(card, i);
        }

        container.addView(itemView);
        return itemView;
    }


    @Override
    public int getCount() {
        return themesId.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
