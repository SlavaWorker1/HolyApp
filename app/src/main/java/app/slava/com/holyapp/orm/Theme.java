package app.slava.com.holyapp.orm;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "themes", id = "_id")
public class Theme extends Model {

    @Column(name = "theme")
    public String theme;

    public Theme() {
    }

    public Theme(String theme) {
        this.theme = theme;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public List<Versus> versuses() {
        return getMany(Versus.class, "theme_id");
    }

    public static List<Theme> getAll() {
        return new Select()
                .from(Theme.class)
                .orderBy("_id ASC")
                .execute();
    }

}
