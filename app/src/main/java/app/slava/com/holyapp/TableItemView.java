package app.slava.com.holyapp;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;


public class TableItemView extends View {

    float itemTextSize;
    int itemTextColor, itemColor, itemSize;
    String itemText;
    Paint painColorStyle;


    public TableItemView(Context context, AttributeSet attrs) {
        super(context, attrs);

        painColorStyle = new Paint();
        TypedArray attributesArray = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.TableItemView, 0, 0);

        try {
            itemText = attributesArray.getString(R.styleable.TableItemView_text);
            itemTextColor = attributesArray.getInteger(R.styleable.TableItemView_textColor,0);
            itemColor = attributesArray.getInteger(R.styleable.TableItemView_itemColor,0);
            itemTextSize = attributesArray.getFloat(R.styleable.TableItemView_textSize,16);
            itemSize = attributesArray.getInteger(R.styleable.TableItemView_itemSize, 150);

        }finally {
            attributesArray.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        painColorStyle.setStyle(Paint.Style.FILL);
        painColorStyle.setAntiAlias(true);
        painColorStyle.setColor(itemColor);

        int centerX = this.getMeasuredWidth()/2;
        int centerY = this.getMeasuredHeight()/2;

        canvas.drawCircle(centerX,centerY, itemSize,painColorStyle);

        painColorStyle.setColor(itemTextColor);
        painColorStyle.setTextAlign(Paint.Align.CENTER);

        canvas.drawText(itemText,centerX,centerY,painColorStyle);
    }

    public void setItemTextSize(float itemTextSize) {
        this.itemTextSize = itemTextSize;
        invalidate();
        requestLayout();
    }

    public void setItemTextColor(int itemTextColor) {
        this.itemTextColor = itemTextColor;
        invalidate();
        requestLayout();
    }

    public int getItemSize() {
        return itemSize;
    }

    public void setItemSize(int itemSize) {
        this.itemSize = itemSize;
    }

    public void setItemColor(int itemColor) {
        this.itemColor = itemColor;
        invalidate();
        requestLayout();
    }

    public void setItemText(String itemText) {
        this.itemText = itemText;
        invalidate();
        requestLayout();
    }
}
