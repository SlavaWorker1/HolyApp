package app.slava.com.holyapp.orm;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "versus", id = "_id")
public class Versus extends Model {

    @Column (name = "text")
    public String text;

    @Column (name = "theme_id")
    public String theme_id;

    @Column (name = "isLiked")
    public int isLiked;

    public Versus() { }

    public Versus(String text, String theme_id, String _id, int isLiked) {
        this.text = text;
        this.theme_id = theme_id;
        this.isLiked = isLiked;
    }

    public static List<Versus> fromCategories(Long categoryId) {
        return new Select()
                .from(Versus.class)
                .where("theme_id = ?", categoryId)
                .orderBy("_id")
                .execute();
    }

    public static List<Versus> getLiked() {
        return new Select()
                .from(Versus.class)
                .where("isLiked = ?", 1)
                .orderBy("_id")
                .execute();
    }

    public void setIsLiked(int isLiked) {
        this.isLiked = isLiked;
    }

}
