package app.slava.com.holyapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridLayout;
import android.widget.ImageView;

import java.util.List;

import app.slava.com.holyapp.orm.Theme;

public class MainFragment extends Fragment {

    ImageView imgBackdrop;
    GridLayout gridLayout;
    final int INTENT_FAVORITE = 12;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_scrolling_main, container, false);
        gridLayout = (GridLayout) rootView.findViewById(R.id.gridLayout);
        List<Theme> themes = Theme.getAll();

        FloatingActionButton fab;

        fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ScrollingActivity.class);
                intent.putExtra("theme_id", Long.valueOf(INTENT_FAVORITE)) ;
                startActivity(intent);
            }
        });


        for (int i = 0; i < themes.size()-1; i++) {
            final ItemView itemView = new ItemView(getContext());

            if ((i - 1 ) % 3 == 0) itemView.setPadding(4,2,4,2);
            else itemView.setPadding(0,2,0,2);
            itemView.setBackgroundColor(getResources().getColor(R.color.backgroundOrange));

            imgBackdrop = (ImageView)rootView.findViewById(R.id.imgBackdrop);
            Bitmap bmpBackdrop = UtilsApp.decodeSampledBitmapFromDrawable(
                getActivity(),
                R.drawable.backdrop03,
                imgBackdrop.getMaxWidth(),
                imgBackdrop.getMaxHeight()
            );

            imgBackdrop.setImageBitmap(bmpBackdrop);

            int reqWidth = itemView.getParentWidth() / 3 - 8;
            int reqHeight = itemView.getParentHeight() / 4;
            int arrImage = UtilsApp.arrImages[i];
            Bitmap img = UtilsApp.decodeSampledBitmapFromDrawable(
                getActivity(),
                arrImage,
                reqWidth,
                reqHeight
            );
            itemView.addImage(img);
            itemView.addText(themes.get(i).theme);
            itemView.setTag(themes.get(i).getId());
            gridLayout.addView(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ScrollingActivity.class);
                    intent.putExtra("theme_id", (Long) v.getTag());
                    startActivity(intent);
                }
            });
        }
        return  rootView;
    }



    @Override
    public void onDestroy() {
        unbindDrawables(imgBackdrop);
        super.onDestroy();
    }

    public void unbindDrawables(View view) {
        try {
            if (view.getBackground() != null)
                view.getBackground().setCallback(null);

            if (view instanceof ImageView) {
                ImageView imageView = (ImageView) view;
                imageView.setImageBitmap(null);
            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++)
                    unbindDrawables(viewGroup.getChildAt(i));

                if (!(view instanceof AdapterView))
                    viewGroup.removeAllViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
