package app.slava.com.holyapp;

import android.app.Application;

import com.activeandroid.ActiveAndroid;


public class ApplicationSettings extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }
}
