package app.slava.com.holyapp;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class ScrollingActivity extends AppCompatActivity {

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.t_pager);


        Long theme_id = getIntent().getLongExtra("theme_id", -1);

        List<Long> themeIds = new ArrayList<>();
        for (int i = 0; i < 13; i++){
            themeIds.add((long) i);
        }

        viewPager = (ViewPager)findViewById(R.id.viewPager);

        SamplePagerAdapter adapter = new SamplePagerAdapter(themeIds, this, theme_id);

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(Integer.valueOf(String.valueOf(theme_id)));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                viewPager.requestLayout();
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }
}
