package app.slava.com.holyapp;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;

public class UtilsApp {
    public static int[] arrImages = {R.drawable.courage,R.drawable.death,R.drawable.forgiviness,
                                R.drawable.faith, R.drawable.family,  R.drawable.encoragement,
                                R.drawable.friendship, R.drawable.joy, R.drawable.life,
                                R.drawable.love, R.drawable.relationship, R.drawable.strength,
                                R.mipmap.ic_favorite_on};

    public static Bitmap decodeSampledBitmapFromDrawable(Context activity, int res, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(activity.getResources(), res , options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inScaled = false;
        return BitmapFactory.decodeResource(activity.getResources(), res, options);
    }


    public static int parentWidth(Context context){
        return  context.getApplicationContext().getResources().getDisplayMetrics().widthPixels;
    }

    public static int parentHeight(Context context){
        return context.getApplicationContext().getResources().getDisplayMetrics().heightPixels;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;

        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int px = Math.round(dp * (metrics.densityDpi / 160f));
        return px;
    }


}
