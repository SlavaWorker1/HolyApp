package app.slava.com.holyapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Update;

import app.slava.com.holyapp.orm.Theme;
import app.slava.com.holyapp.orm.Versus;

public class Card extends CardView{

    private String shareText;
    private Context context;
    private TextView tvText;
    private ImageView imgLike, imgShare;
    private boolean isClicked = false;
    private  LinearLayout linearLayout;
    private Theme favoriteTheme;
    private Versus versus;

    public Card(Context context, Versus versus, LinearLayout linearLayout) {
        super(context);
        this.context = context;
        this.shareText = versus.text;
        this.versus = versus;
        this.favoriteTheme = Theme.load(Theme.class, 12);
        this.linearLayout = linearLayout;
        init();
    }

    public Card(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Card(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.card, this);
        tvText = (TextView)findViewById(R.id.tvText);
        imgLike = (ImageView)findViewById(R.id.imgLike);
        imgShare = (ImageView)findViewById(R.id.imgShare);
        imgLike.setTag(versus);


        if (versus.isLiked == 1) {
            imgLike.setImageResource(R.mipmap.ic_favorite_on);
        }
        else imgLike.setImageResource(R.mipmap.ic_favorite);
        isClicked = (versus.isLiked == 1);

        imgShare.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareText);
                getContext().startActivity(Intent.createChooser(sharingIntent, "title"));
            }
        });


        imgLike.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Versus _versus = (Versus) v.getTag();
                isClicked=!isClicked;
                imgLike.setImageResource(isClicked ? R.mipmap.ic_favorite_on : R.mipmap.ic_favorite);
                _versus.setIsLiked((isClicked) ? 1 : 0);
                _versus.save();

                if (isClicked) {
                    if (!favoriteTheme.versuses().contains(_versus)) {
                        Versus versus = new Versus();
                        versus.theme_id = String.valueOf(12);
                        versus.text = _versus.text;
                        versus.setIsLiked(1);
                        versus.save();
                    }
                } else  {
                    linearLayout.removeView(Card.this);
                    new Delete()
                            .from(Versus.class)
                            .where("theme_id = ? and text = ?", 12 , versus.text)
                            .execute();

                    new Update(Versus.class)
                            .set("isLiked = ?", 0)
                            .where("text = ?", versus.text)
                            .execute();


                    requestLayout();
                }

                for (int i = 0 ; i < favoriteTheme.versuses().size(); i++) {
                    Log.d("favorite", favoriteTheme.versuses().get(i).text);
                }


            }
        });
    }

    public TextView getTvText(){
        return tvText;
    }

}
